###
#   Testumgebung Webseite mit PHP, Adminer und MySQL Umgebung

module "web" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "web-61"
  userdata   = "cloud-init-php.yaml"
  depends_on  = [ module.mysql ]   
  ports      = [22, 80]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"    
}

module "mysql" {
  source     = "git::https://github.com/mc-b/terraform-lerncloud-multipass"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-aws"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-azure"
  #source     = "git::https://github.com/mc-b/terraform-lerncloud-maas"
  module     = "mysql"
  userdata   = "cloud-init-mysql.yaml"
  ports      = [22, 3306]
  
  # Server Access Info
  url      = "${var.url}"
  key      = "${var.key}"
  vpn      = "${var.vpn}"    
}
